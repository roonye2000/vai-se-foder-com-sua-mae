using System;

namespace Ronnye__Chain_of_responsability
{
    public class Director : Aprobador
    {
        public override void Procesar(Compra c)
        {
            Console.WriteLine(string.Format("Compra aprobada por el {0}", this.GetType().Name)); 

        }
    }
}
